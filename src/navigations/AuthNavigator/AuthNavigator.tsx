import {
    createStackNavigator,
    StackNavigationOptions
} from '@react-navigation/stack';
import { AuthNavigatorParamsList } from './AuthNavigator.types';
import { Image, TouchableOpacity } from 'react-native';
import { icons } from '../../assets';
import { styles } from './AuthNavigator.styles';
import { LoginScreen, RegisterScreen } from '../../screens';
import { useNavigation } from '@react-navigation/native';

const Stack = createStackNavigator<AuthNavigatorParamsList>();

export function AuthNavigator() {
    const { goBack } = useNavigation();

    const headerLeft = () => (
        <TouchableOpacity onPress={() => goBack()}>
            <Image source={icons.BACK_ARROW} style={styles.goBackIcon} />
        </TouchableOpacity>
    );

    const headerOptions: StackNavigationOptions = {
        headerShadowVisible: false,
        headerBackTitleVisible: false
    };

    return (
        <Stack.Navigator>
            <Stack.Group>
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{ ...headerOptions, headerShown: false }}
                />
                <Stack.Screen
                    name="Register"
                    component={RegisterScreen}
                    options={{
                        ...headerOptions,
                        headerLeft,
                        headerTransparent: true,
                        title: ''
                    }}
                />
            </Stack.Group>
        </Stack.Navigator>
    );
}
