import { NavigatorScreenParams } from '@react-navigation/native';

export type AuthNavigatorParamsList = {
    Login?: NavigatorScreenParams<undefined>;
    Register: NavigatorScreenParams<undefined>;
    PrivacyPolicies: NavigatorScreenParams<undefined>;
};
