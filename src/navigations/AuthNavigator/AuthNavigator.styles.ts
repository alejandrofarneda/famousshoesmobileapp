import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    goBackIcon: {
        width: 24,
        height: 24,
        tintColor: colors.white,
        marginLeft: 18
    }
});
