import { StyleSheet } from 'react-native';
import { colors } from '../../../utils';

export const styles = (isFocused?: boolean) =>
    StyleSheet.create({
        tabBarContainer: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.background.primary,
            height: 55
        },
        content: {
            flex: 1,
            alignItems: 'center'
        },
        icon: {
            height: isFocused ? 24 : 18,
            width: isFocused ? 24 : 18,
            tintColor: isFocused ? colors.font.warning : colors.font.primary,
            opacity: isFocused ? 1 : 0.5
        },
        text: {
            color: colors.font.primary,
            fontSize: isFocused ? 12 : 9,
            fontWeight: isFocused ? '700' : '500',
            opacity: isFocused ? 1 : 0.5
        }
    });
