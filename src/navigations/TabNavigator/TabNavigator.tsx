import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { icons } from '../../assets';
import { SettingsScreen, UserProfileScreen } from '../../screens';
import { TabNavigatorParamList } from './TabNavigator.types';
import { CustomTabBar } from './CustomTabBar';
import { HomeNavigator } from '../StackNavigators';
import { UserProfileNavigator } from '../StackNavigators/UserProfileNavigator';

const Tab = createBottomTabNavigator<TabNavigatorParamList>();

export function TabNavigator() {
    return (
        <Tab.Navigator
            tabBar={CustomTabBar}
            screenOptions={{
                headerShown: false
            }}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeNavigator}
                options={{
                    title: 'Home',
                    tabBarIcon: icons.HOME_TAB,
                    tabBarTestID: 'tab-home'
                }}
            />
            <Tab.Screen
                name="UserProfileStack"
                component={UserProfileNavigator}
                options={{
                    title: 'Profile',
                    tabBarIcon: icons.USER_TAB,
                    tabBarTestID: 'tab-profile'
                }}
            />
            <Tab.Screen
                name="Settings"
                component={SettingsScreen}
                options={{
                    title: 'Settings',
                    tabBarIcon: icons.SETTINGS_TAB,
                    tabBarTestID: 'tab-settings'
                }}
            />
        </Tab.Navigator>
    );
}
