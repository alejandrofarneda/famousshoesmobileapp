export type TabNavigatorParamList = {
    HomeStack: {
        Home: undefined;
        Product: undefined;
    };
    UserProfileStack: {
        UserProfile: undefined;
        ChangeEmail: undefined;
        ChangePassword: undefined;
    };
    Settings: undefined;
};
