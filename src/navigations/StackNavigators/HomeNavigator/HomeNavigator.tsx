import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, TouchableOpacity } from 'react-native';
import { icons } from '../../../assets';
import { useNavigation } from '@react-navigation/native';
import { styles } from './HomeNavigator.styles';
import { HomeScreen, ProductScreen } from '../../../screens';
import { ProductsProvider } from '../../../contexts/';
import { HomeNavigatorParamsList } from './HomeNavigator.types';
import { useAuth } from '../../../hooks';
import { forSlide } from '../NavigatorUtils';

const Stack = createStackNavigator<HomeNavigatorParamsList>();

export function HomeNavigator() {
    const {
        state: { accessToken }
    } = useAuth();
    const { goBack } = useNavigation();

    const headerLeft = () => (
        <TouchableOpacity
            onPress={() => goBack()}
            style={styles.headerLeftContainer}
        >
            <Image source={icons.BACK_ARROW} style={styles.goBackIcon} />
        </TouchableOpacity>
    );

    return (
        <ProductsProvider accessToken={accessToken}>
            <Stack.Navigator>
                <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        headerStyle: styles.headerContainer,
                        headerTitle: 'CRUD-HOME',
                        headerTitleStyle: [styles.title, { marginLeft: 20 }],
                        headerTitleAlign: 'left',
                        headerShadowVisible: false,
                        headerBackTitleVisible: false
                    }}
                />
                <Stack.Screen
                    name="Product"
                    component={ProductScreen}
                    options={{
                        headerStyle: styles.headerContainer,
                        headerTitle: 'PRODUCT',
                        headerTitleAlign: 'left',
                        headerTitleStyle: styles.title,
                        headerLeft,
                        headerShadowVisible: false,
                        headerBackTitleVisible: false,
                        cardStyleInterpolator: forSlide
                    }}
                />
            </Stack.Navigator>
        </ProductsProvider>
    );
}
