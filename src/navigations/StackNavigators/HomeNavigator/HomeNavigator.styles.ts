import { StyleSheet } from 'react-native';
import { colors } from '../../../utils';

export const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: colors.background.primary,
        height: 80
    },
    title: {
        color: colors.white,
        opacity: 0.6,
        fontWeight: '600',
        fontSize: 28
    },
    headerLeftContainer: {
        marginLeft: 18
    },
    goBackIcon: {
        width: 26,
        height: 26,
        tintColor: colors.white,
        opacity: 0.7
    }
});
