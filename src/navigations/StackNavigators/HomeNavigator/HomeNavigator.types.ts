import { NavigatorScreenParams } from '@react-navigation/native';

export type HomeNavigatorParamsList = {
    Home?: NavigatorScreenParams<undefined>;
    Product?: NavigatorScreenParams<undefined>;
};
