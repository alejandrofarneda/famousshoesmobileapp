import { NavigatorScreenParams } from '@react-navigation/native';

export type UserProfileNavigatorParamsList = {
    UserProfile?: NavigatorScreenParams<undefined>;
    ChangeEmail?: NavigatorScreenParams<undefined>;
    ChangePassword?: NavigatorScreenParams<undefined>;
};
