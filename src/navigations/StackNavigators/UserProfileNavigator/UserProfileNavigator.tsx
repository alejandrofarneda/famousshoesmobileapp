import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, TouchableOpacity } from 'react-native';
import { icons } from '../../../assets';
import { useNavigation } from '@react-navigation/native';
import { styles } from './UserProfileNavigator.styles';
import {
    UserProfileScreen,
    ChangeEmailScreen,
    ChangePasswordScreen
} from '../../../screens';
import { UserProfileNavigatorParamsList } from './UserProfileNavigator.types';
import { forSlide } from '../NavigatorUtils';

const Stack = createStackNavigator<UserProfileNavigatorParamsList>();

export function UserProfileNavigator() {
    const { navigate } = useNavigation();

    const headerLeft = () => (
        <TouchableOpacity
            onPress={() =>
                navigate('UserProfileStack', { screen: 'UserProfile' })
            }
            style={styles.headerLeftContainer}
        >
            <Image source={icons.BACK_ARROW} style={styles.goBackIcon} />
        </TouchableOpacity>
    );

    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName="UserProfile"
        >
            <Stack.Screen name="UserProfile" component={UserProfileScreen} />
            <Stack.Screen
                name="ChangeEmail"
                component={ChangeEmailScreen}
                options={{
                    headerShown: true,
                    title: 'CHANGE EMAIL',
                    headerTitleStyle: styles.title,
                    headerStyle: styles.headerContainer,
                    headerLeft,
                    headerShadowVisible: false,
                    headerBackTitleVisible: false,
                    cardStyleInterpolator: forSlide
                }}
            />

            <Stack.Screen
                name="ChangePassword"
                component={ChangePasswordScreen}
                options={{
                    headerShown: true,
                    title: 'CHANGE PASSWORD',
                    headerTitleStyle: styles.title,
                    headerStyle: styles.headerContainer,
                    headerLeft,
                    headerShadowVisible: false,
                    headerBackTitleVisible: false,
                    cardStyleInterpolator: forSlide
                }}
            />
        </Stack.Navigator>
    );
}
