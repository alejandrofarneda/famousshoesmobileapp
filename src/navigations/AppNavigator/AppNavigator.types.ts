import { NavigatorScreenParams } from '@react-navigation/native';

export type AppNavigatorParamsList = {
    Tab?: NavigatorScreenParams<undefined>;
    LegalInformation: NavigatorScreenParams<undefined>;
    ShippingInformation: NavigatorScreenParams<undefined>;
};
