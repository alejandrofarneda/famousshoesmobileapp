import { createStackNavigator } from '@react-navigation/stack';
import { AppNavigatorParamsList } from './AppNavigator.types';
import { TabNavigator } from '../TabNavigator';

const Stack = createStackNavigator<AppNavigatorParamsList>();

export function AppNavigator() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Group
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="Tab" component={TabNavigator} />
            </Stack.Group>
        </Stack.Navigator>
    );
}
