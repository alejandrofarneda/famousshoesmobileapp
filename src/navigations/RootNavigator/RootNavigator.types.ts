import { NavigatorScreenParams } from '@react-navigation/native';

export type RootNavigatorParamList = {
    Auth?: NavigatorScreenParams<undefined>;
    App?: NavigatorScreenParams<undefined>;
};
