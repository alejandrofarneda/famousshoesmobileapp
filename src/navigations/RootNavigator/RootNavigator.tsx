import { createStackNavigator } from '@react-navigation/stack';
import { ModalLoading } from '../../components';
import { useAuth } from '../../hooks';
import { AppNavigator } from '../AppNavigator';
import { AuthNavigator } from '../AuthNavigator';
import { RootNavigatorParamList } from './RootNavigator.types';

const Stack = createStackNavigator<RootNavigatorParamList>();

export function RootNavigator() {
    const { state } = useAuth();

    return (
        <>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                {!state.accessToken ? (
                    <Stack.Screen name="Auth" component={AuthNavigator} />
                ) : (
                    <Stack.Screen name="App" component={AppNavigator} />
                )}
            </Stack.Navigator>
            <ModalLoading text="Loading" visible={state.isLoading} />
        </>
    );
}
