import { FormikValues, useFormik } from 'formik';
import { AnySchema } from 'yup';

type UseFormProps<T> = {
    initialValues: T;
    validationSchema: AnySchema | (() => AnySchema);
    onSubmit: (values: T) => void | Promise<any>;
};

export const useForm = <T extends FormikValues = FormikValues>(
    props: UseFormProps<T>
) => {
    const { initialValues, validationSchema, onSubmit } = props;

    const formik = useFormik({
        initialValues,
        validationSchema,
        validateOnChange: false,
        onSubmit
    });

    return formik;
};
