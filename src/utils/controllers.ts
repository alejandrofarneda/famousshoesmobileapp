import { API_URL } from '@env';
import { UserModel } from '../models';

export const getMe = async (accessToken: string): Promise<UserModel> => {
    try {
        const response = await fetch(`${API_URL}/user/me`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        });

        const result = await response.json();

        if (response.status !== 200) throw result;

        delete result.password;

        return {
            id: result._id,
            email: result.email,
            role: result.role,
            color: result.color,
            avatar: result.avatar || null
        };
    } catch (error) {
        throw error;
    }
};
