export const colors = {
    warning: '#FF0000',
    white: '#FFFFFF',
    black: '#F000000',
    border: {
        primary: '#9EA3B0'
    },
    background: {
        primary: '#1B2021'
    },
    font: {
        primary: '#C9C9C9',
        link: '#3685B5',
        warning: '#FF6666'
    }
};
