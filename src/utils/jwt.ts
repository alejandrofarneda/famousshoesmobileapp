import { API_URL } from '@env';
import decoded from 'jwt-decode';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TokensType } from '../contexts/AuthContext/AuthContext.types';
import { UserTokensModel } from '../models';

export async function setStorageToken(
    type: 'access' | 'refresh',
    token: string
): Promise<void> {
    try {
        await AsyncStorage.setItem(type, token);
    } catch (error) {
        if (error instanceof Error) {
            console.error(error.message);
        }
    }
}

export async function getStorageToken(
    type: 'access' | 'refresh'
): Promise<string> {
    try {
        const token = await AsyncStorage.getItem(type);
        return token;
    } catch (error) {
        if (error instanceof Error) {
            console.error(error.message);
        }
    }
}

export const refreshAccessToken = async (
    tokens: TokensType
): Promise<string> => {
    const newAccessToken = await getNewAccessToken(tokens.refresh);

    tokens.access = newAccessToken;
    setStorageToken('access', tokens.access);

    return tokens.access;
};

export const hasExpiredToken = (token: string): boolean => {
    const { exp }: UserTokensModel = decoded(token);
    const currentData = new Date().getTime();

    if (exp <= currentData) {
        return true;
    }

    return false;
};

export async function cleanStorageTokens() {
    try {
        await AsyncStorage.removeItem('access');
        await AsyncStorage.removeItem('refresh');
    } catch (error) {
        if (error instanceof Error) {
            console.error(error.message);
        }
    }
}

export const getNewAccessToken = async (
    refreshToken: string
): Promise<string> => {
    const response = await fetch(`${API_URL}/auth/refresh_access_token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${refreshToken}`
        }
    });

    const newToken = await response.json();

    return newToken;
};
