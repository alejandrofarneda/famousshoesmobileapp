export type ProductModel = {
    id: string;
    name: string;
    brand: string;
    info: string;
    image: string;
    price: number;
    stock: boolean;
};
