export type UserTokensModel = {
    token_type: 'access' | 'refresh';
    user_id: string;
    iat: number;
    exp: number;
};

export type UserModel = {
    id: string;
    email: string;
    role: string;
    color: string;
    avatar: string;
};
