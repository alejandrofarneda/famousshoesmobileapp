import React, { useEffect, useState } from 'react';
import { Keyboard, Pressable, Text, View } from 'react-native';
import { TextInput } from '../../components';
import { useAuth, useForm } from '../../hooks';
import { styles } from './ChangeEmailScreen.styles';
import { initialValues, validationSchema } from './change-email-form-data';
import { FormValueTypes } from './ChangeEmailScreen.types';
import { useNavigation } from '@react-navigation/native';

export function ChangeEmailScreen() {
    const { state } = useAuth();
    const [errorInfo, setErrorInfo] = useState(null);
    const { navigate } = useNavigation();

    useEffect(() => {
        if (state.error) setErrorInfo(state.error.message);
    }, [state.error]);

    const changeEmail = async (email: string) => Promise.resolve({});

    const onSubmit = async (formValue: FormValueTypes) => {
        Keyboard.dismiss();
        onClearErrors();

        try {
            await changeEmail(formValue.email.trim().toLowerCase());
        } catch (error: any) {
            console.log(error);

            setErrorInfo(error.message);
        }
    };

    const form = useForm({ initialValues, validationSchema, onSubmit });

    const onClearErrors = () => {
        form.setErrors({});
    };

    return (
        <View testID="login-form" style={styles.container}>
            <Text style={styles.title}>EMAIL: </Text>
            <TextInput
                placeholder="johndoe@example.com"
                onChangeText={(text) => {
                    onClearErrors();
                    form.setFieldValue(
                        'email',
                        text.toLocaleLowerCase().trim()
                    );
                }}
                value={form.values.email}
                keyboardType="email-address"
                onFocus={onClearErrors}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('email', '');
                }}
                infoText={form.errors.email || errorInfo}
            />

            <Pressable
                onPress={() => {
                    form.handleSubmit;
                    navigate('UserProfileStack', { screen: 'UserProfile' });
                }}
                style={({ pressed }) => [
                    styles.buttonContainer,
                    pressed && { backgroundColor: '#000000' }
                ]}
            >
                <Text style={styles.buttonText}>CHANGE EMAIL</Text>
            </Pressable>
        </View>
    );
}
