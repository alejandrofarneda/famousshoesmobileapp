import * as Yup from 'yup';

export const initialValues = {
    email: 'reyagustin@gmail.com'
};

export const validationSchema = Yup.object({
    email: Yup.string().email('Invalid Email').required('Email required')
});
