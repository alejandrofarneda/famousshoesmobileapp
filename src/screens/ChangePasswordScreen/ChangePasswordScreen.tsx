import React, { useEffect, useState } from 'react';
import { Keyboard, Pressable, Text, View } from 'react-native';
import { TextInput } from '../../components';
import { useAuth, useForm } from '../../hooks';
import { styles } from './ChangePasswordScreen.styles';
import { initialValues, validationSchema } from './change-password-form-data';
import { FormValueTypes } from './ChangePasswordScreen.types';
import { useNavigation } from '@react-navigation/native';

export function ChangePasswordScreen() {
    const { state } = useAuth();
    const { navigate } = useNavigation();
    const [hiddenOldPassword, setHiddenOldPassword] = useState(true);
    const [hiddenNewPassword, setHiddenNewPassword] = useState(true);
    const [errorInfo, setErrorInfo] = useState(null);

    useEffect(() => {
        if (state.error) setErrorInfo(state.error.message);
    }, [state.error]);

    const changePassword = async (oldPassword: string, newPassword: string) => {
        Promise.resolve({});
    };

    const onSubmit = async (formValue: FormValueTypes) => {
        Keyboard.dismiss();
        onClearErrors();

        try {
            await changePassword(formValue.oldPassword, formValue.newPassword);

            navigate('UserProfileStack', { screen: 'UserProfile' });
        } catch (error: any) {
            console.log(error.message);

            setErrorInfo(error);
        }
    };

    const form = useForm({ initialValues, validationSchema, onSubmit });

    const onClearErrors = () => {
        form.setErrors({});
    };

    return (
        <View testID="login-form" style={styles.container}>
            <Text style={styles.title}>OLD PASSWORD</Text>
            <TextInput
                placeholder="password..."
                value={form.values.oldPassword}
                onChangeText={(text: string) => {
                    onClearErrors();
                    form.setFieldValue('oldPassword', text);
                }}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('oldPassword', '');
                }}
                onFocus={onClearErrors}
                onShowHidden={() => {
                    setHiddenOldPassword((prev) => !prev);
                }}
                secureTextEntry={hiddenOldPassword}
                infoText={
                    form.errors.oldPassword ||
                    form.errors.newPassword ||
                    errorInfo
                }
            />

            <Text style={styles.title}>NEW PASSWORD</Text>
            <TextInput
                placeholder="passwords must be same..."
                value={form.values.newPassword}
                onChangeText={(text: string) => {
                    onClearErrors();
                    form.setFieldValue('newPassword', text);
                }}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('newPassword', '');
                }}
                onFocus={onClearErrors}
                onShowHidden={() => {
                    setHiddenNewPassword((prev) => !prev);
                }}
                secureTextEntry={hiddenNewPassword}
            />

            <Pressable
                onPress={() => form.handleSubmit}
                style={({ pressed }) => [
                    styles.buttonContainer,
                    pressed && { backgroundColor: '#000000' }
                ]}
            >
                <Text style={styles.buttonText}>CHANGE PASSWORD</Text>
            </Pressable>
        </View>
    );
}
