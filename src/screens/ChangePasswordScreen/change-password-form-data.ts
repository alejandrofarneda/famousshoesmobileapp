import * as Yup from 'yup';

export const initialValues = {
    oldPassword: 'Ceco222.',
    newPassword: ''
};

export const validationSchema = Yup.object({
    oldPassword: Yup.string().max(20).min(6).required('Password required'),
    newPassword: Yup.string().max(20).min(6).required('Password required')
});
