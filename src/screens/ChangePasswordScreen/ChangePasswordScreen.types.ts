export type FormValueTypes = {
    oldPassword: string;
    newPassword: string;
};
