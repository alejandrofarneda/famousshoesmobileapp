import {
    Dimensions,
    Keyboard,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Pressable,
    View
} from 'react-native';
import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useHeaderHeight } from '@react-navigation/elements';
import { BackgroundTabsLayout, LoginForm } from '../../components';
import { styles } from './LoginScreen.styles';
import { colors } from '../../utils';

export const LoginScreen = () => {
    const headerHeight = useHeaderHeight();
    const screenHeight = Dimensions.get('window').height - headerHeight;

    const { navigate } = useNavigation();
    const goToRegister = () => navigate('Register');

    return (
        <BackgroundTabsLayout style={styles.container}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={{ height: screenHeight }}>
                    <Text style={styles.title}>WELCOME SHOE FANATIC</Text>
                    <LoginForm />
                    <TouchableOpacity
                        style={styles.registerContainer}
                        onPress={goToRegister}
                    >
                        <Text style={styles.registerText}>
                            Still without an account:
                        </Text>
                        <Text style={styles.registerLink}>{' REGISTER'}</Text>
                    </TouchableOpacity>
                </View>
            </TouchableWithoutFeedback>
        </BackgroundTabsLayout>
    );
};
