import React from 'react';
import { Text, View } from 'react-native';
import { BackgroundTabsLayout, MenuCard } from '../../components';
import { styles } from './SettingsScreen.styles';

export const SettingsScreen = () => {
    const userButtons = [
        {
            title: 'Privacy Policies',
            onPress: () => console.log('Navigate PrivacyPiliciesScreen')
        },
        {
            title: 'Help',
            onPress: () => console.log('Navigate HelpScreen')
        },
        {
            title: 'Version X.X.X(X)',
            secondaryText: 'New version available',
            onPress: () => console.log('UpdateAppAction')
        }
    ];

    return (
        <BackgroundTabsLayout style={styles.container}>
            {userButtons.map((item, index) => (
                <MenuCard
                    title={item.title}
                    onPress={item.onPress}
                    secondaryText={item.secondaryText}
                    key={item.title + index.toString()}
                />
            ))}
        </BackgroundTabsLayout>
    );
};
