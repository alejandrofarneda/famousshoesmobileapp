import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = ({ color } = { color: '' }) =>
    StyleSheet.create({
        container: {
            marginTop: 50,
            justifyContent: 'space-between'
        },
        userLogo: {
            borderRadius: 50,
            height: 100,
            width: 100,
            alignSelf: 'flex-start',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: color
        },
        userLetter: {
            textTransform: 'capitalize',
            fontSize: 40,
            color: colors.font.primary
        },
        logoutContainer: {
            backgroundColor: colors.warning,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 'auto',
            marginBottom: 16
        },
        text: {
            fontSize: 20,
            color: colors.white,
            fontWeight: 'bold'
        }
    });
