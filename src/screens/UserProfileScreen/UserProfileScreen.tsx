import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Text, View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { BackgroundTabsLayout, MenuCard } from '../../components';
import { useAuth } from '../../hooks';
import { styles } from './UserProfileScreen.styles';

export const UserProfileScreen = () => {
    const { logout } = useAuth();
    const { navigate } = useNavigation();
    const style = styles();

    const userButtons = [
        {
            title: 'Change email',
            onPress: () => navigate('ChangeEmail')
        },
        {
            title: 'Change password',
            onPress: () => navigate('ChangePassword')
        },
        {
            title: 'Manage permissions',
            onPress: () => console.log('Navigate ManagePermissionsScreen')
        }
    ];

    return (
        <BackgroundTabsLayout style={style.container}>
            <View>
                {userButtons.map((item, index) => (
                    <MenuCard
                        title={item.title}
                        onPress={item.onPress}
                        key={item.title + index.toString()}
                    />
                ))}
            </View>
            <TouchableHighlight style={style.logoutContainer} onPress={logout}>
                <Text style={style.text}>LOGOUT</Text>
            </TouchableHighlight>
        </BackgroundTabsLayout>
    );
};
