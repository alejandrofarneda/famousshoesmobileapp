import { ProgressBarAndroidBase, StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        justifyContent: 'space-between',
        paddingHorizontal: 14,
        flex: 1
    },
    title: {
        fontSize: 26,
        paddingTop: 50,
        textAlign: 'center',
        fontWeight: '500',
        color: colors.warning
    },
    subtitle: {
        fontSize: 24,
        paddingTop: 20,
        textAlign: 'center'
    },
    registerContainer: {
        flexDirection: 'row',
        alignItems: 'baseline',
        alignSelf: 'center',
        marginTop: 'auto'
    },
    registerText: {
        color: colors.font.primary,
        fontSize: 16
    },
    registerLink: {
        fontWeight: '500',
        color: colors.font.link,
        fontSize: 18,
        paddingBottom: 12
    }
});
