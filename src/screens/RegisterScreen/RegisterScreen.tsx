import {
    Dimensions,
    Keyboard,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import React from 'react';
import { BackgroundTabsLayout, RegisterForm } from '../../components';
import { useHeaderHeight } from '@react-navigation/elements';
import { styles } from './RegisterScreen.styles';
import { useNavigation } from '@react-navigation/native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const RegisterScreen = () => {
    const headerHeight = useHeaderHeight();
    const screenHeight = Dimensions.get('window').height - headerHeight;

    const { navigate } = useNavigation();
    const goToLogin = () => navigate('Login');

    return (
        <BackgroundTabsLayout style={styles.container}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <View style={{ height: screenHeight }}>
                    <RegisterForm />
                    <TouchableOpacity
                        style={styles.registerContainer}
                        onPress={goToLogin}
                    >
                        <Text style={styles.registerText}>
                            I have an account:
                        </Text>
                        <Text style={styles.registerLink}>{' LOGIN'}</Text>
                    </TouchableOpacity>
                </View>
            </TouchableWithoutFeedback>
        </BackgroundTabsLayout>
    );
};
