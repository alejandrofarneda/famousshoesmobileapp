import React, { useState } from 'react';
import { FlatList } from 'react-native';
import {
    BackgroundTabsLayout,
    ModalLoading,
    ProductCard
} from '../../components';
import { useProducts } from '../../hooks';
import { styles } from './HomeScreen.styles';
import { useNavigation } from '@react-navigation/native';
import { ProductsNotFoundCard } from './ProductNotFoundCard';

const PRODUCT_ITEM_HEIGHT = 255;
const PRODUCT_ITEM_OFFSET = 5;
const PRODUCT_ITEM_MARGIN = PRODUCT_ITEM_OFFSET * 2;

export const HomeScreen = () => {
    const { state, deleteProduct, editProductStock } = useProducts();
    const { navigate } = useNavigation();

    return (
        <BackgroundTabsLayout>
            {state.error && state.isLoading && <ProductsNotFoundCard />}
            {state.products.length >= 0 && !state.error && (
                <FlatList
                    style={styles.listContainer}
                    data={state.products}
                    keyExtractor={(item, index) => item.id + index.toString()}
                    renderItem={({ item }) => (
                        <ProductCard
                            item={item}
                            onChangeStock={() => editProductStock(item)}
                            onPressDelete={deleteProduct}
                            onNavigate={() =>
                                navigate('Product', {
                                    product: item
                                })
                            }
                        />
                    )}
                    getItemLayout={(data, index) => {
                        const productHeight =
                            PRODUCT_ITEM_HEIGHT + PRODUCT_ITEM_MARGIN;
                        return {
                            length: productHeight,
                            offset: productHeight * index,
                            index
                        };
                    }}
                    numColumns={2}
                />
            )}
            <ModalLoading visible={state.isLoading} text="Loading" />
        </BackgroundTabsLayout>
    );
};
