import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        padding: 5,
        marginBottom: 8
    },
    title: {
        fontSize: 20,
        color: colors.font.primary,
        marginTop: 20,
        marginBottom: 30,
        textAlign: 'center',
        fontWeight: '600'
    }
});
