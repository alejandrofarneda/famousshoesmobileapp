import { Image, Text, View } from 'react-native';
import { icons } from '../../../assets';
import { styles } from './ProductNotFoundCard.styles';

export function ProductsNotFoundCard() {
    const item = {
        id: 0,
        title: "COULDN'T RETRIEVE INFORMATION FROM SERVER",
        info: 'Please try again later'
    };
    return (
        <View style={styles.container}>
            <View style={styles.cardContainer}>
                <Image source={icons.ERROR_404} style={styles.errorImage} />
                <Text style={styles.errorText}>{item.title}</Text>
                <Text style={styles.errorSubText}>{item.info}</Text>
            </View>
        </View>
    );
}
