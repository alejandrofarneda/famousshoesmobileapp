import { StyleSheet } from 'react-native';
import { colors } from '../../../utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    cardContainer: {
        alignSelf: 'center',
        justifyContent: 'space-evenly',
        borderRadius: 3,
        width: 250,
        height: 250,
        padding: 10
    },
    errorText: {
        fontSize: 16,
        lineHeight: 25,
        textAlign: 'center',
        color: colors.font.primary,
        opacity: 0.8,
        alignSelf: 'center',
        fontWeight: '700'
    },
    errorSubText: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.font.warning,
        alignSelf: 'center',
        fontWeight: '700'
    },
    errorImage: {
        width: 90,
        height: 90,
        alignSelf: 'center',
        resizeMode: 'contain'
    }
});
