import { ProductModel } from '../../models';

export type Props = {
    route: {
        params: {
            product: ProductModel;
        };
    };
};
