import { API_IMAGES } from '@env';
import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';
import { BackgroundTabsLayout, ModalDelete } from '../../components';
import { useProducts } from '../../hooks';
import { styles } from './ProductScreen.styles';
import { Props } from './ProductScreen.types';

export const ProductScreen = ({ route }: Props) => {
    const { navigate } = useNavigation();
    const { image, brand, name, price, info, id } = route.params.product;
    const { deleteProduct } = useProducts();
    const [modalDelete, setModalDelete] = useState(false);

    return (
        <BackgroundTabsLayout style={styles.container}>
            <Text style={styles.title}>{name}</Text>
            <Text style={styles.itemText}>{brand}</Text>
            <Image
                source={{ uri: `${API_IMAGES}/${image}` }}
                style={styles.itemImage}
            />
            <Text style={styles.descriptionText}>{info}</Text>
            <Text style={styles.descriptionText}>Price: $ {price},00</Text>
            <TouchableOpacity onPress={() => setModalDelete(true)}>
                <Text style={styles.trashText}>{'Remove Item'}</Text>
            </TouchableOpacity>
            <ModalDelete
                modalVisible={modalDelete}
                onDelete={() => {
                    deleteProduct(id);
                    navigate('Home');
                }}
                closeModal={() => setModalDelete(false)}
            />
        </BackgroundTabsLayout>
    );
};
