import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 8
    },
    title: {
        fontSize: 40,
        color: colors.font.warning,
        marginBottom: 30,
        textAlign: 'center',
        fontWeight: '600'
    },
    itemText: {
        fontSize: 30,
        flexWrap: 'wrap',
        color: colors.font.primary,
        fontWeight: '500',
        textAlign: 'center'
    },
    itemImage: {
        width: 300,
        height: 220,
        resizeMode: 'contain',
        borderRadius: 5,
        alignSelf: 'center'
    },
    descriptionText: {
        marginTop: 20,
        fontSize: 20,
        color: colors.font.primary,
        fontWeight: '500',
        textAlign: 'center'
    },
    trashText: {
        marginTop: 40,
        fontSize: 25,
        padding: 5,
        paddingHorizontal: 40,
        borderRadius: 5,
        color: colors.font.primary,
        fontWeight: '500',
        alignSelf: 'center',
        backgroundColor: colors.warning,
        textAlign: 'center'
    }
});
