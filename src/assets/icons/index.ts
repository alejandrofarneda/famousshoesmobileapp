import HOME_TAB from './home.png';
import SETTINGS_TAB from './settings.png';
import USER_TAB from './user.png';
import ERROR_404 from './icon-404.png';
import BACK_ARROW from './back-arrow.png';
import CROSS_CLOSE from './close_cross.png';
import CROSS_ROUNDED from './cross_rounded_icon.png';
import HIDDEN from './hidden_off_visibility_icon.png';
import RIGHT_ARROW from './right_arrow.png';
import TRASH from './trash.png';

export const icons = {
    HOME_TAB,
    SETTINGS_TAB,
    USER_TAB,
    ERROR_404,
    BACK_ARROW,
    CROSS_CLOSE,
    CROSS_ROUNDED,
    HIDDEN,
    RIGHT_ARROW,
    TRASH
};
