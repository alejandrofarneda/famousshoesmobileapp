import { ProductModel } from '../../models';

export namespace IContext {
    export type Context = {
        state: State;
        deleteProduct: (productId: string) => Promise<void>;
        editProductStock: (product: ProductModel) => void;
    };
    // addProduct: (product: ProductModel) => void;
    // editProduct: (product: ProductModel) => void;

    export type State = {
        products?: ProductModel[] | [];
        isLoading?: boolean;
        error?: Error;
    };

    export type Action =
        | { type: 'loading' }
        | { type: 'success'; payload: State }
        | { type: 'skip' }
        | { type: 'error'; payload: State };

    export type Props = Record<string, unknown> &
        React.HTMLAttributes<HTMLElement> & {
            accessToken: string;
        };
}
