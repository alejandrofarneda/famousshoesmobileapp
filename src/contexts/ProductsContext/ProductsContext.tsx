import React, { createContext, useEffect, useReducer } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { IContext } from './ProductsContext.types';
import { API_URL } from '@env';
import { ProductModel } from '../../models';

const initialState: IContext.State = {
    isLoading: false,
    products: [],
    error: null
};

export const ProductsContext = createContext<IContext.Context>({
    state: initialState,
    deleteProduct: (_productId: string) => null,
    editProductStock: (_product: ProductModel) => null
    // addProduct: (_product: ProductModel) => null,
    // editProduct: (_product: ProductModel) => null
});

export function ProductsProvider(props: IContext.Props) {
    const { children, accessToken } = props;
    const isFocused = useIsFocused();
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        if (isFocused) {
            getProducts();
        }
    }, [isFocused]);

    const getProducts = async (): Promise<void> => {
        dispatch({ type: 'loading' });
        try {
            const response = await fetch(`${API_URL}/products`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${accessToken}`
                }
            });
            const productsResponse = await response.json();

            if (response.status !== 200) throw productsResponse;

            const productsToDomain = () => {
                const products = productsResponse.map((item: any) => {
                    return {
                        id: item._id,
                        ...item
                    };
                });
                return products;
            };

            dispatch({
                type: 'success',
                payload: { products: productsToDomain() }
            });
        } catch (error: any) {
            console.error(error);
            dispatch({ type: 'error', payload: error.message });
        }
    };

    const editProductStock = async (product: ProductModel) => {
        dispatch({ type: 'loading' });

        const editedProduct: ProductModel = {
            ...product,
            stock: product.stock ? false : true
        };

        try {
            const response = await fetch(`${API_URL}/product/${product.id}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${accessToken}`
                },
                body: JSON.stringify(editedProduct)
            });

            const result = await response.json();
            if (response.status !== 200) throw result;

            getProducts();
        } catch (error: any) {
            console.error(error);
            dispatch({ type: 'error', payload: error.message });
        }
    };

    const deleteProduct = async (productId: string) => {
        dispatch({ type: 'loading' });

        try {
            const response = await fetch(`${API_URL}/product/${productId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${accessToken}`
                }
            });
            const result = await response.json();
            if (response.status !== 200) {
                throw result;
            }

            getProducts();
        } catch (error) {
            if (error instanceof Error) {
                console.error(error);
                dispatch({ type: 'error', payload: { error: error } });
            }
        }
    };

    const productsData = {
        state,
        deleteProduct,
        editProductStock
    };

    return (
        <ProductsContext.Provider value={productsData}>
            {children}
        </ProductsContext.Provider>
    );
}

function reducer(
    state: IContext.State,
    action: IContext.Action
): IContext.State {
    switch (action.type) {
        case 'loading':
            return {
                ...state,
                isLoading: true
            };
        case 'success':
            const data = action.payload;
            return {
                ...state,
                ...data,
                isLoading: false
            };
        case 'skip':
            return {
                ...state,
                isLoading: false,
                error: null
            };
        case 'error':
        default:
            const error = action.payload.error;
            return {
                ...initialState,
                error
            };
    }
}

// const deleteProduct = (productId: number) => {
//     setIsLoading(true);

//     fetch(url + `/${productId}`, {
//         method: 'DELETE',
//         headers
//     })
//         .then((res) => res.json())
//         .then((resJson) => {
//             console.log('delete:', resJson);
//             getProducts();
//         })
//         .catch((e) => {
//             console.log(e);
//         });
// };

// const addProduct = (product: Product) => {
//     setIsLoading(true);

//     fetch(url, {
//         method: 'POST',
//         headers,
//         body: JSON.stringify(product)
//     })
//         .then((res) => res.json())
//         .then((resJson) => {
//             console.log('post:', resJson);
//             getProducts();
//         })
//         .catch((e) => {
//             console.log(e);
//         });
// };

// const newProduct = {
//     name: 'Michael Jordan',
//     model: 'Air Jordan 1985',
//     image: 'https://siterg.uol.com.br/wp-content/uploads/2021/10/121208127_-1x-1.jpeg',
//     price: 1500,
//     stock: true,
//     id: 8
// };

// const editProduct = (product: Product) => {
//     setIsLoading(true);

//     fetch(url + `/${product.id}`, {
//         method: 'PUT',
//         headers,
//         body: JSON.stringify({
//             product
//         })
//     })
//         .then((res) => res.json())
//         .then((resJson) => {
//             console.log('updated:', resJson);
//             getProducts();
//         })
//         .catch((e) => {
//             console.log(e);
//         });
// };

// const deleteProduct = null;
// const editProductStock = null;
// const addProduct = null;
// const editProduct = null;
