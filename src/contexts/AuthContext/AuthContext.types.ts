import React from 'react';
import { UserModel } from '../../models';

export namespace IAuthContext {
    export type Context = {
        state: State;
        register(email: string, password: string): Promise<void>;
        login(email: string, password: string): Promise<void>;
        logout(): Promise<void>;
    };

    export type Props = Record<string, unknown> &
        React.HTMLAttributes<HTMLElement>;

    export type State = {
        isLoading: boolean;
        accessToken: string | null;
        refreshToken: string | null;
        user: UserModel | null;
        error: Error | null;
    };

    export type Action =
        | { type: 'loading' }
        | { type: 'success'; payload: any }
        | { type: 'error'; payload: any }
        | { type: 'skip' }
        | { type: 'logout' };

    export type Reducer<S, A> = (state: S, action: A) => S;
}

export type TokensType = {
    access: string;
    refresh: string;
};
