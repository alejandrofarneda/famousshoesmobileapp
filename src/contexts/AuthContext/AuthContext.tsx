import { API_URL } from '@env';
import { createContext, useEffect, useReducer } from 'react';
import {
    cleanStorageTokens,
    getMe,
    getStorageToken,
    hasExpiredToken,
    refreshAccessToken,
    setStorageToken
} from '../../utils';
import { IAuthContext, TokensType } from './AuthContext.types';

const initialState: IAuthContext.State = {
    isLoading: false,
    user: null,
    accessToken: null,
    refreshToken: null,
    error: null
};

export const AuthContext = createContext<IAuthContext.Context>({
    state: initialState,
    login: async () => {
        //
    },
    register: async () => {
        //
    },
    logout: async () => {
        //
    }
});

export function AuthProvider(props: IAuthContext.Props) {
    const { children } = props;
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const initAsync = async () => {
            try {
                const accessToken = await getStorageToken('access');
                const refreshToken = await getStorageToken('refresh');

                if (!accessToken || !refreshToken) {
                    dispatch({ type: 'skip' });
                    return;
                }

                if (hasExpiredToken(refreshToken)) {
                    dispatch({ type: 'skip' });
                    return;
                }

                if (!hasExpiredToken(accessToken)) {
                    const user = await getMe(accessToken);

                    const newState = {
                        ...state,
                        user,
                        accessToken,
                        refreshToken
                    };
                    dispatch({ type: 'success', payload: newState });
                } else {
                    const newAccessToken = await refreshAccessToken({
                        access: accessToken,
                        refresh: refreshToken
                    });
                    const user = await getMe(newAccessToken);

                    const newState = { newAccessToken, refreshToken, user };

                    dispatch({
                        type: 'success',
                        payload: newState
                    });
                }
            } catch (error) {
                console.error(error);

                dispatch({ type: 'error', payload: error });
            }
        };

        initAsync();
    }, []);

    const login = async (email: string, password: string) => {
        dispatch({ type: 'loading' });

        try {
            const response = await fetch(`${API_URL}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password })
            });

            const result = await response.json();

            if (response.status !== 200) throw result;

            setStorageToken('access', result.access);
            setStorageToken('refresh', result.refresh);

            const user = await getMe(result.access);

            const payload: any = {
                accessToken: result.access,
                refreshToken: result.refresh,
                user
            };

            dispatch({ type: 'success', payload });
        } catch (error) {
            console.error(error);

            dispatch({ type: 'error', payload: error });
        }
    };

    const register = async (email: string, password: string) => {
        dispatch({ type: 'loading' });

        try {
            const response = await fetch(`${API_URL}/auth/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password })
            });

            const result: TokensType = await response.json();

            if (response.status !== 200) throw result;

            const payload: any = {
                accessToken: result.access,
                refreshToken: result.refresh
            };

            dispatch({ type: 'success', payload });
        } catch (error) {
            console.error(error);

            dispatch({ type: 'error', payload: error });
        }
    };

    const logout = async () => {
        dispatch({ type: 'loading' });

        await cleanStorageTokens();

        dispatch({ type: 'logout' });
    };

    const result = {
        state,
        register,
        login,
        logout
    };

    return (
        <AuthContext.Provider value={result}>{children}</AuthContext.Provider>
    );
}

function reducer(
    state: IAuthContext.State,
    action: IAuthContext.Action
): IAuthContext.State {
    switch (action.type) {
        case 'loading':
            return { ...state, isLoading: true };
        case 'success':
            const session = action.payload;

            return {
                ...state,
                ...session,
                isLoading: false
            };
        case 'skip':
            return {
                ...state,
                isLoading: false,
                error: null
            };
        case 'logout':
            return {
                ...initialState,
                isLoading: false,
                error: null
            };
        case 'error':
        default:
            return {
                ...state,
                isLoading: false,
                error: action.payload
            };
    }
}
