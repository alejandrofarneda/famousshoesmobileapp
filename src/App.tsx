import 'react-native-gesture-handler';
import React from 'react';
import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { RootNavigator } from './navigations';
import { registerRootComponent } from 'expo';
import { AuthProvider } from './contexts';
import { StatusBar } from 'react-native';
import { colors } from './utils';

function App() {
    const NavigatorBackgroundTheme = {
        ...DefaultTheme,
        colors: {
            ...DefaultTheme.colors,
            background: '#000000'
        }
    };

    return (
        <>
            <StatusBar backgroundColor={'#000000'} barStyle="light-content" />
            <AuthProvider>
                <NavigationContainer theme={NavigatorBackgroundTheme}>
                    <RootNavigator />
                </NavigationContainer>
            </AuthProvider>
        </>
    );
}

export default registerRootComponent(App);
