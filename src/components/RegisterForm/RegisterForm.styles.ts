import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 12,
        paddingTop: 50
    },
    title: {
        fontSize: 18,
        color: colors.font.primary
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: colors.font.primary
    },
    buttonDisabledContainer: {
        backgroundColor: colors.black,
        borderWidth: 0.2,
        borderColor: colors.font.primary
    },
    buttonText: {
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 1.3,
        color: colors.black
    },
    buttonDisabledText: {
        color: colors.font.primary,
        opacity: 0.5
    },
    policiesContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    policiesText: {
        color: colors.font.primary,
        fontSize: 16,
        fontWeight: '600'
    },
    policiesLink: {
        color: colors.font.link,
        fontSize: 16,
        fontWeight: '600'
    }
});
