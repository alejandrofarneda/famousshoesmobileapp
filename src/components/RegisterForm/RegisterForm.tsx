import React, { useEffect, useState } from 'react';
import {
    Keyboard,
    Pressable,
    Switch,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { useAuth, useForm } from '../../hooks';
import { FormValueTypes } from './RegisterForm.types';
import { initialValues, validationSchema } from './register-form-data';
import { styles } from './RegisterForm.styles';
import { TextInput } from '../TextInput';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { colors } from '../../utils';

export function RegisterForm() {
    const [hiddenPassword, setHiddenPassword] = useState(true);
    const [hiddenPasswordRepeat, setHiddenPasswordRepeat] = useState(true);
    const [errorInfo, setErrorInfo] = useState(null);
    const { register, state } = useAuth();
    const { navigate } = useNavigation();

    const onSubmit = async (formValue: FormValueTypes) => {
        Keyboard.dismiss();
        onClearErrors();

        try {
            await register(
                formValue.email.trim().toLowerCase(),
                formValue.password
            );

            if (state.error) throw state.error;

            navigate('Login');
        } catch (error: any) {
            console.log({ error: state.error });

            setErrorInfo(error.message);
        }
    };

    const form = useForm({ initialValues, validationSchema, onSubmit });

    const onClearErrors = () => {
        form.setErrors({});
        setErrorInfo(null);
    };

    return (
        <View testID="login-form" style={styles.container}>
            <Text style={styles.title}>EMAIL</Text>
            <TextInput
                placeholder="johndoe@example.com"
                onChangeText={(text) => {
                    onClearErrors();
                    form.setFieldValue(
                        'email',
                        text.toLocaleLowerCase().trim()
                    );
                }}
                value={form.values.email}
                keyboardType="email-address"
                onFocus={onClearErrors}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('email', '');
                }}
                infoText={
                    form.errors.email ||
                    form.errors.password ||
                    form.errors.passwordRepeat ||
                    form.errors.policy ||
                    errorInfo
                }
            />

            <Text style={styles.title}>PASSWORD</Text>
            <TextInput
                placeholder="password..."
                value={form.values.password}
                onChangeText={(text: string) => {
                    onClearErrors();
                    form.setFieldValue('password', text);
                }}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('password', '');
                }}
                onFocus={onClearErrors}
                onShowHidden={() => {
                    setHiddenPassword((prev) => !prev);
                }}
                secureTextEntry={hiddenPassword}
            />

            <Text style={styles.title}>REPEAT PASSWORD</Text>
            <TextInput
                placeholder="passwords must be same..."
                value={form.values.passwordRepeat}
                onChangeText={(text: string) => {
                    onClearErrors();
                    form.setFieldValue('passwordRepeat', text);
                }}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('passwordRepeat', '');
                }}
                onFocus={onClearErrors}
                onShowHidden={() => {
                    setHiddenPasswordRepeat((prev) => !prev);
                }}
                secureTextEntry={hiddenPasswordRepeat}
            />
            <View style={styles.policiesContainer}>
                <Switch
                    value={form.values.policy}
                    trackColor={{ true: colors.font.link }}
                    thumbColor={colors.font.primary}
                    onTouchStart={() =>
                        form.setFieldValue('policy', !form.values.policy)
                    }
                />
                <TouchableWithoutFeedback
                    onPress={() =>
                        form.setFieldValue('policy', !form.values.policy)
                    }
                >
                    <Text style={styles.policiesText}>Agree with owr </Text>
                </TouchableWithoutFeedback>
                <TouchableOpacity
                    onPress={() => console.log('Navigate to PoliciesScreen')}
                >
                    <Text style={styles.policiesLink}>Privacy Policies</Text>
                </TouchableOpacity>
            </View>

            <Pressable
                onPress={form.handleSubmit}
                style={({ pressed }) => [
                    styles.buttonContainer,
                    pressed && { backgroundColor: '#F000000' }
                ]}
            >
                <Text style={styles.buttonText}>Register</Text>
            </Pressable>
        </View>
    );
}
