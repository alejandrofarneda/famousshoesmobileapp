export type FormValueTypes = {
    email: string;
    password: string;
    passwordRepeat: string;
};
