import * as Yup from 'yup';

export const initialValues = {
    email: 'reyagustin@gmail.com',
    password: 'Ceco222.',
    passwordRepeat: 'Ceco222.',
    policy: false
};

export const validationSchema = Yup.object({
    email: Yup.string().email('Invalid Email').required('Email required'),
    password: Yup.string().max(20).min(6).required('Password required'),
    passwordRepeat: Yup.string()
        .required('Password required')
        .oneOf([Yup.ref('password')], 'Passwords must be same'),
    policy: Yup.boolean()
        .required('To continue, you must read and accept the Privacy Policy')
        .isTrue('To continue, you must read and accept the Privacy Policy')
});
