import { ReactNode } from 'react';
import { ViewStyle } from 'react-native';

export type BackgroundTabsLayoutTypes = {
    children: ReactNode;
    style?: ViewStyle;
};
