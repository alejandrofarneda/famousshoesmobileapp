import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { styles } from './BackgroundTabsLayout.styles';
import { BackgroundTabsLayoutTypes } from './BackgroundTabsLayout.types';

export const BackgroundTabsLayout = (props: BackgroundTabsLayoutTypes) => {
    const { children, style } = props;

    return (
        <SafeAreaView style={[styles.container, style]}>
            {children}
        </SafeAreaView>
    );
};
