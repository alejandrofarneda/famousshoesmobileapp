import React from 'react';
import { Modal, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../utils';
import { styles } from './ModalView.styles';
import { Props } from './ModalView.types';

export const ModalView = ({
    children,
    title,
    onSubmit,
    cancelable,
    visible = false,
    onDismiss,
    submitText = 'Ok'
}: Props) => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onDismiss={onDismiss}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Text style={styles.modalText}>{title}</Text>
                    <View>{children}</View>
                    <View
                        style={{
                            alignSelf: 'flex-end',
                            alignItems: 'center',
                            flexDirection: 'row'
                        }}
                    >
                        {cancelable && (
                            <TouchableOpacity
                                style={{
                                    ...styles.button,
                                    backgroundColor: colors.white
                                }}
                                onPress={onDismiss}
                            >
                                <Text
                                    style={[
                                        styles.textStyle,
                                        { color: colors.font.warning }
                                    ]}
                                >
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                        )}
                        {onSubmit && (
                            <TouchableOpacity
                                style={styles.button}
                                onPress={onSubmit}
                            >
                                <Text style={styles.textStyle}>
                                    {submitText}
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            </View>
        </Modal>
    );
};
