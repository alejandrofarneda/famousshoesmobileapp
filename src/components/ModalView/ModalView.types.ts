import { ReactNode } from 'react';

export type Props = {
    children: ReactNode;
    title: string;
    onSubmit(): void;
    cancelable: boolean;
    visible: boolean;
    onDismiss(): void;
    submitText: string;
};
