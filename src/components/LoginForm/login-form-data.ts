import * as Yup from 'yup';

export const initialValues = {
    email: 'reyagustin@gmail.com',
    password: 'Ceco222.'
};

export const validationSchema = Yup.object({
    email: Yup.string().email('Invalid Email').required('Email required'),
    password: Yup.string().max(20).min(6).required('Password required')
});
