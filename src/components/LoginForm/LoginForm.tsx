import React, { useEffect, useState } from 'react';
import { Keyboard, Pressable, Text, View } from 'react-native';
import { useAuth, useForm } from '../../hooks';
import { FormValueTypes } from './LoginForm.types';
import { initialValues, validationSchema } from './login-form-data';
import { styles } from './LoginForm.styles';
import { TextInput } from '../TextInput';

export function LoginForm() {
    const { login, state } = useAuth();
    const [hiddenPassword, setHiddenPassword] = useState(true);
    const [errorInfo, setErrorInfo] = useState(null);
    useEffect(() => {
        if (state.error) setErrorInfo(state.error.message);
    }, [state.error]);

    const onSubmit = async (formValue: FormValueTypes) => {
        Keyboard.dismiss();
        onClearErrors();

        try {
            await login(
                formValue.email.trim().toLowerCase(),
                formValue.password
            );
        } catch (error: any) {
            console.log(error);

            setErrorInfo(error.message);
        }
    };

    const form = useForm({ initialValues, validationSchema, onSubmit });

    const onClearErrors = () => {
        form.setErrors({});
        setErrorInfo(null);
    };

    return (
        <View testID="login-form" style={styles.container}>
            <Text style={styles.title}>EMAIL</Text>
            <TextInput
                placeholder="johndoe@example.com"
                onChangeText={(text) => {
                    onClearErrors();
                    form.setFieldValue(
                        'email',
                        text.toLocaleLowerCase().trim()
                    );
                }}
                value={form.values.email}
                keyboardType="email-address"
                onFocus={onClearErrors}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('email', '');
                }}
                infoText={
                    form.errors.email || form.errors.password || errorInfo
                }
            />

            <Text style={styles.title}>PASSWORD</Text>
            <TextInput
                placeholder="password..."
                value={form.values.password}
                onChangeText={(text: string) => {
                    onClearErrors();
                    form.setFieldValue('password', text);
                }}
                onClearText={() => {
                    onClearErrors();
                    form.setFieldValue('password', '');
                }}
                onFocus={onClearErrors}
                onShowHidden={() => {
                    setHiddenPassword((prev) => !prev);
                }}
                secureTextEntry={hiddenPassword}
            />

            <Pressable
                onPress={form.handleSubmit}
                style={({ pressed }) => [
                    styles.buttonContainer,
                    pressed && { backgroundColor: '#000000' }
                ]}
            >
                <Text style={styles.buttonText}>LOGIN</Text>
            </Pressable>
        </View>
    );
}
