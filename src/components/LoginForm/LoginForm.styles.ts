import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 12,
        paddingTop: 50
    },
    title: {
        fontSize: 18,
        color: colors.font.primary
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: colors.font.primary,
        marginBottom: 30
    },
    buttonText: {
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 1.3,
        color: colors.black
    },
    buttonDisabledText: {
        color: colors.font.primary,
        opacity: 0.5
    }
});
