export type Props = {
    modalVisible: boolean;
    closeModal: () => void;
    onDelete: () => void;
};
