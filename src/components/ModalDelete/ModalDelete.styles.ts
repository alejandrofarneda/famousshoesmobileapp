import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.black
    },
    modalView: {
        margin: 20,
        width: '70%',
        paddingVertical: 20,
        backgroundColor: colors.white,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalTitle: {
        paddingVertical: 12,
        textAlign: 'center',
        fontSize: 22,
        paddingHorizontal: 15,
        fontWeight: '700'
    },
    modalTask: {
        paddingTop: 6,
        textAlign: 'center',
        fontSize: 18,
        paddingHorizontal: 25
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 40,
        width: '100%',
        marginTop: 20
    },
    deleteButtonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 110,
        borderRadius: 4,
        backgroundColor: '#D9D0DE',
        marginHorizontal: 6
    },
    deleteButtonText: {
        color: colors.warning,
        fontWeight: '700',
        fontSize: 18
    },
    cancelButtonText: {
        color: colors.black,
        fontWeight: '400',
        fontSize: 18
    }
});
