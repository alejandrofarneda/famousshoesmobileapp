import React from 'react';
import { View, Modal, Text, Pressable } from 'react-native';
import { styles } from './ModalDelete.styles';
import { Props } from './ModalDelete.types';

export function ModalDelete({ closeModal, modalVisible, onDelete }: Props) {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            statusBarTranslucent={true}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Text style={styles.modalTitle}>Delete product</Text>

                    <Text style={styles.modalTask}>
                        This action will remove definitely the product
                    </Text>
                    <View style={styles.buttonsContainer}>
                        <Pressable
                            style={styles.deleteButtonContainer}
                            onPress={() => {
                                onDelete();
                                closeModal();
                            }}
                        >
                            <Text style={styles.deleteButtonText}>Delete</Text>
                        </Pressable>

                        <Pressable
                            style={styles.deleteButtonContainer}
                            onPress={closeModal}
                        >
                            <Text style={styles.cancelButtonText}>Cancel</Text>
                        </Pressable>
                    </View>
                </View>
            </View>
        </Modal>
    );
}
