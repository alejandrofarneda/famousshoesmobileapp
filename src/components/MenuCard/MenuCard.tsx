import React from 'react';
import { Image, Text, TouchableHighlight, View } from 'react-native';
import { icons } from '../../assets';
import { styles } from './MenuCard.styles';

export const MenuCard = ({
    title,
    secondaryText,
    onPress
}: {
    title: string;
    secondaryText?: string;
    onPress(): void;
}) => {
    return (
        <TouchableHighlight style={styles.containerCard} onPress={onPress}>
            <>
                <View>
                    <Text style={styles.titleCard}>{title}</Text>
                    {secondaryText && (
                        <Text style={styles.secondaryText}>
                            {secondaryText}
                        </Text>
                    )}
                </View>
                <Image source={icons.RIGHT_ARROW} style={styles.arrow} />
            </>
        </TouchableHighlight>
    );
};
