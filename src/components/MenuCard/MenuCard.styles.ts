import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    containerCard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 12,
        width: '100%',
        backgroundColor: colors.background.primary,
        height: 60,
        marginBottom: 6
    },
    titleCard: {
        color: colors.white,
        fontSize: 16,
        paddingLeft: 10
    },
    secondaryText: {
        color: colors.white,
        opacity: 0.4,
        fontSize: 14,
        paddingLeft: 10
    },
    arrow: {
        width: 18,
        height: 18,
        tintColor: colors.white
    }
});
