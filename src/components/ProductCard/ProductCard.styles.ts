import { StyleSheet } from 'react-native';
import { colors } from '../../utils';
import { StyleProps } from './ProductCard.types';

export const styles = ({
    SCREEN_WIDTH,
    PRODUCT_ITEM_HEIGHT,
    PRODUCT_ITEM_OFFSET,
    PRODUCT_ITEM_MARGIN
}: StyleProps) => {
    const itemWidth =
        (SCREEN_WIDTH - PRODUCT_ITEM_MARGIN) / 2 - PRODUCT_ITEM_MARGIN;

    return StyleSheet.create({
        item: {
            margin: PRODUCT_ITEM_OFFSET,
            overflow: 'hidden',
            borderRadius: 3,
            width: itemWidth,
            height: PRODUCT_ITEM_HEIGHT,
            flexDirection: 'column',
            borderColor: colors.border.primary,
            borderWidth: 1
        },
        itemImage: {
            width: itemWidth,
            height: 125,
            resizeMode: 'contain',
            justifyContent: 'center',
            alignItems: 'center'
        },
        itemTitle: {
            fontSize: 20,
            color: colors.font.primary,
            margin: PRODUCT_ITEM_OFFSET * 2,
            alignSelf: 'center',
            fontWeight: 'bold'
        },
        stockContainer: {
            flexDirection: 'row',
            alignItems: 'center',
            height: 50,
            paddingLeft: 4
        },
        stockText: {
            paddingLeft: 10,
            fontSize: 16,
            color: colors.font.primary,
            fontWeight: 'bold'
        },
        trashText: {
            marginTop: 7,
            fontSize: 15,
            width: '100%',
            color: colors.font.primary,
            fontWeight: 'bold',
            alignSelf: 'center',
            backgroundColor: colors.warning,
            textAlign: 'center'
        }
    });
};
