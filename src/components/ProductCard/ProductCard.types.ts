import { ProductModel } from '../../models';

export type ProductCardProps = {
    item: ProductModel;
    onChangeStock(): void;
    onNavigate(): void;
    onPressDelete: (productId: string) => Promise<void>;
};

export type StyleProps = {
    SCREEN_WIDTH: number;
    PRODUCT_ITEM_HEIGHT: number;
    PRODUCT_ITEM_OFFSET: number;
    PRODUCT_ITEM_MARGIN: number;
};
