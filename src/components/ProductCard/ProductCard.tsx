import React, { useState } from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import { styles } from './ProductCard.styles';
import { ProductCardProps } from './ProductCard.types';
import { API_IMAGES } from '@env';
import { ModalDelete } from '../ModalDelete';

const { width, height } = Dimensions.get('window');

const SCREEN_WIDTH = width < height ? width : height;
const PRODUCT_ITEM_HEIGHT = 255;
const PRODUCT_ITEM_OFFSET = 5;
const PRODUCT_ITEM_MARGIN = PRODUCT_ITEM_OFFSET * 2;

export const ProductCard = (props: ProductCardProps) => {
    const { item, onChangeStock, onNavigate, onPressDelete } = props;
    const [modalDelete, setModalDelete] = useState(false);

    const styled = styles({
        SCREEN_WIDTH,
        PRODUCT_ITEM_HEIGHT,
        PRODUCT_ITEM_OFFSET,
        PRODUCT_ITEM_MARGIN
    });

    return (
        <>
            <TouchableOpacity style={styled.item} onPress={onNavigate}>
                <Text style={styled.itemTitle}>{item.name}</Text>
                {!item.image ? (
                    <View style={styled.itemImage}>
                        <Text>No image</Text>
                    </View>
                ) : (
                    <Image
                        source={{ uri: `${API_IMAGES}/${item.image}` }}
                        style={styled.itemImage}
                    />
                )}

                <TouchableOpacity
                    onPress={onChangeStock}
                    style={styled.stockContainer}
                >
                    <Text style={styled.stockText}>Stock: </Text>
                    <Text
                        ellipsizeMode="tail"
                        style={[
                            styled.stockText,
                            !item.stock
                                ? {
                                      color: 'red'
                                  }
                                : { color: 'green' }
                        ]}
                    >
                        {item.stock ? 'AVAILABLE' : 'UNAVAILABLE'}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => setModalDelete(true)}>
                    <Text style={styled.trashText}>{'Remove Item'}</Text>
                </TouchableOpacity>
            </TouchableOpacity>
            <ModalDelete
                modalVisible={modalDelete}
                onDelete={() => onPressDelete(item.id)}
                closeModal={() => setModalDelete(false)}
            />
        </>
    );
};
