import { StyleSheet } from 'react-native';
import { colors } from '../../utils';

export const styles = StyleSheet.create({
    container: {
        height: 40,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.border.primary,
        borderRadius: 4,
        paddingLeft: 4,
        marginVertical: 10
    },
    inputField: {
        alignSelf: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%'
    },
    inputText: {
        fontSize: 18,
        width: '90%',
        color: colors.font.primary,
        opacity: 0.7
    },
    endIconsContainer: {
        flexDirection: 'row',
        marginLeft: 4
    },
    endIconLeft: {
        width: 18,
        height: 18,
        tintColor: colors.font.primary,
        opacity: 0.5
    },
    endIconRight: {
        width: 20,
        height: 20,
        marginLeft: 12,
        tintColor: colors.font.primary,
        opacity: 0.5
    },
    infoText: {
        color: colors.warning,
        paddingTop: 10,
        paddingBottom: 20
    }
});
