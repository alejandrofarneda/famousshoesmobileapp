import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View,
    TextInput as NativeTextInput
} from 'react-native';
import { icons } from '../../assets';
import { styles } from './TextInput.styles';
import { TextInputType } from './TextInput.types';

export const TextInput = (props: TextInputType) => {
    const {
        onFocus,
        value,
        onChangeText,
        onClearText,
        onShowHidden,
        infoText,
        placeholder,
        ...rest
    } = props;

    return (
        <>
            <View style={styles.container}>
                <View style={styles.inputField}>
                    <NativeTextInput
                        placeholder={placeholder}
                        style={styles.inputText}
                        value={value}
                        onChangeText={onChangeText}
                        onFocus={onFocus}
                        {...rest}
                    />
                    <View style={styles.endIconsContainer}>
                        {!!value && (
                            <TouchableOpacity onPress={onClearText}>
                                <Image
                                    style={styles.endIconLeft}
                                    source={icons.CROSS_ROUNDED}
                                />
                            </TouchableOpacity>
                        )}
                        {!!value && !!onShowHidden && (
                            <TouchableOpacity onPress={onShowHidden}>
                                <Image
                                    style={styles.endIconRight}
                                    source={icons.HIDDEN}
                                />
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            </View>
            {infoText && <Text style={styles.infoText}>{infoText}</Text>}
        </>
    );
};
