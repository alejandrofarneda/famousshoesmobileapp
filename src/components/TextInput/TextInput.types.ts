import { TextInputProps } from 'react-native';

export type TextInputType = {
    onFocus(): void;
    value: string;
    onChangeText(text: string): void;
    placeholder: string;
    onClearText?(): void;
    onShowHidden?(): void;
    infoText?: string;
} & TextInputProps;
