export type Props = {
    visible: boolean;
    text: string;
};
