import React from 'react';
import { View, Modal, Text, ActivityIndicator } from 'react-native';
import { styles } from './ModalLoading.styles';
import { Props } from './ModalLoading.types';

export function ModalLoading({ visible, text }: Props) {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            statusBarTranslucent={true}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <ActivityIndicator size="large" color={'red'} />
                    <Text style={styles.modalText}>{text}</Text>
                </View>
            </View>
        </Modal>
    );
}
