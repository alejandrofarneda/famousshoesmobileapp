# REACT NATIVE CRUD

## Mobile crud application that manages a unique shoes stock.

» Expo

» ReactNative

» React Navigation

» Typescript

» Hooks

» Context

» Redux

» More...

## Available Scripts

In the project directory, you can run:

`npm start`

or

`yarn start`

Scan QR with expo app to view it in your ANDROID mobile.

The app will reload when you make changes. You may also see any lint errors in
the console.

## Check .env.example

## Coming up next:

» Testing

» Translations
